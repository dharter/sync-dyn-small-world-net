#!/usr/bin/env python
import fileinput
import os
import re
import sys


def find_text_between(src_doc, re_begin, re_end):
    """Find and return text between regular expression 1 re_begin, and regular expression 2 re_end"""
    txt = ""
    in_txt = False
    for line in fileinput.input(src_doc):
        m = re.match(re_begin, line)
        if m:
            in_txt = True
            continue
        m = re.match(re_end, line)
        if m:
            in_txt = False
            break
        if in_txt:
            txt += line
    fileinput.close()
    return txt


def find_line_begins(src_doc, re_begin):
    """Find line that matches regular expression, return first match object in the re"""
    for line in fileinput.input(src_doc):
        m = re.match(re_begin, line)
        if m:
            break
    fileinput.close()
    return m.group(1)


def rewrite_template(dst_doc, tag, txt):
    """Rewrite the tag placeholder in the destination doc with the given txt"""
    # for safety, mv template file before we do anything with it.
    cmd = 'mv %s /tmp/%s' % (dst_doc, dst_doc)
    os.system(cmd)

    # retrieve all lines into memory of the input template file
    in_fn = '/tmp/%s' % dst_doc
    in_f = open(in_fn)
    lines = in_f.readlines()
    in_f.close()

    # write back out to original file, replacing the tage when we see it
    outf = open(dst_doc, 'w')
    for line in lines:
        m = re.match(tag, line)
        if m:
            outf.write(txt)
        else:
            outf.write(line)
    outf.close()

    # remove the no longer needed temporary file we just processed
    #cmd = 'rm /tmp/%s' % dst_doc
    #os.system(cmd)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        sys.stderr.write("usage: %s src-doc.tex  dst-template.tex\n" % sys.argv[0])
        sys.exit(1)

    # documents to get text from, the source, and to place into template, the destination
    src_doc = sys.argv[1]
    dst_doc = sys.argv[2]

    # we will first make passes through source to extract the abstract, keywords 
    # and the document text body
    abstract = find_text_between(src_doc, r'\s*\\begin{abstract}.*', r'\s*\\textit{Keywords}.*')
    keywords = find_line_begins(src_doc, r's*\\textit{Keywords}:\s*(.*)')
    doc_txt = find_text_between(src_doc, r'\s*\\end{abstract}.*', r'\s*\\bibliography.*')

    # now rewrite the template, filling in the abstract, keywords and document text
    rewrite_template(dst_doc, r'\s*\%abstract-text-here\s*', abstract)
    rewrite_template(dst_doc, r'\s*\%keywords-text-here\s*', keywords)
    rewrite_template(dst_doc, r'\s*\%document-text-here\s*', doc_txt)
