#!/usr/bin/env python
#
# post processing of a .ipynb conversion.  Basically only keep the exports and function definitions we need.
import fileinput
import re

func_to_keep = ['create_wsinterpolated_network', 
                'calc_avg_clustering',
                'calc_avg_wiring_length',
                'simulate_network',
                'draw_firings_activity_plot',
                'create_delay_matrix',
                'simulate_network_delay',
                'convolve_firings', 
                'add_convoluted_firings',
                'calc_power_spectrum',
                'max_freq_power',
                ]
modules_not_to_import = []
in_func = False

for line in fileinput.input():

    # keep all from x import x 
    match = re.search('from .* import .*', line)
    if match:
        print match.group()

    # keep all import x as y, except those we don't need on cluster
    match = re.search('import (.*) as .*', line)
    if match:
        export = True
        if match.group(1) not in modules_not_to_import:
            print match.group()

    # look for start of function
    match = re.search('def ([a-zA-Z_0-9]+)\(.*\):.*', line)
    if match:
        if match.group(1) in func_to_keep:
            print ''
            print match.group()
            in_func = True
        else:
            in_func = False

    # if we are inside of a function def, keep printing lines that start with 4 spaces (or more) or are blank
    # the first line after function is another function def, or a comment or code outside of a def
    if in_func:
        if line[:4] == '    ':
            print line[:-1]
        elif line[:-1] == '':
            print line[:-1]
        elif line[:3] == 'def':
            pass
        else:
            in_func = False
            print ''

# add a little function to be loaded on remote that we can use to determine if scripts loaded correctly
s = """def exp_script_ready():
    return True
"""
print s

