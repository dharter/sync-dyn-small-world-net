# there is a bug in matplotlib .eps generation currently, this removes the
# postscript line causing the bug.  Remove this when matplotlib/ghostscript get fixed?

PROJ=synchronization-dynamics-small-world
PICS=`grep includegraphics $PROJ.tex | sed 's/.*figures\/\([^}]*\).*/\1/'`

for pic in $PICS
do
    pic=`basename -s .png $pic`
    sed -i '/^end$/d' ../figures/$pic.eps
done
