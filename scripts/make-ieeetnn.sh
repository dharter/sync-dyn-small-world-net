#!/bin/bash
PROJ=synchronization-dynamics-small-world

# setup build directory and files
mkdir ieeetnn
cd ieeetnn
cp ../$PROJ.tex source-doc.tex
cp ../styles/ieeetnn-template.tex $PROJ.tex
cp ../$PROJ.bib .
cp ../styles/IEEEtran.cls .
cp ../styles/IEEEtran.bst .

# fill in template
../../scripts/fill-template.py source-doc.tex $PROJ.tex

# copy figures and fix figure references
sed -i 's/\.png//g' $PROJ.tex 
PICS=`grep includegraphics $PROJ.tex | sed 's/.*\(figures\/[^}]*\).*/\1/'`
for pic in $PICS
do
    cp ../../$pic.eps .
    # there is a bug in matplotlib .eps generation currently, this removes the
    # postscript line causing the bug.  Remove this when matplotlib/ghostscript get fixed?
    sed -i '/^end$/d' $pic.eps
done
sed -i 's/\.\.\/figures/\./g' $PROJ.tex

# compile the latex document to pdf
latex $PROJ
bibtex $PROJ
latex $PROJ
latex $PROJ
dvips -t letterSize -Ppdf -G0 $PROJ -t letter -Pwww
ps2pdf -dSubsetFonts=true -dEmbedAllFonts=true -dMaxSubsetPct=100 -dCompatibilityLevel=1.3 $PROJ.ps

rm -f *.ps *.out *.log *.dvi *.blg *.aux
