from numpy import * 
from numpy.random import *
from sklearn.utils import shuffle
from scipy.signal import convolve
from random import randrange
import matplotlib.pyplot as plt
import networkx as nx

def create_wsinterpolated_network(Ne, Ni, k, p, we=25, wi=50):
    """Create a network of Izhikevich units.  The network will have Ne excitatory units and
    Ni inhibitory units. Arrange the units randomly around a ring lattice where each unit is 
    connected to its k nearest neighbors.  Perform Watts & Strogatz interpolation: with
    probability p each original connection is randomly rewired to a different unit.
    """
    N = Ne + Ni # total number of units we will create

    # set up Excitatory parameters
    re = rand(Ne,1)
    ae = ones( (Ne, 1) ) * 0.02
    be = ones( (Ne, 1) ) * 0.2
    ce = -65 + 15 * re**2.0
    de = 8 - 6 * re**2.0
    se = arange(0, Ne).reshape(Ne, 1) # indices of the excitatory units
    
    # set up Inhibitory parameters
    ri = rand(Ni,1)
    ai = 0.02 + 0.08 * ri
    bi = 0.25 - 0.05 * ri
    ci = -65 * ones( (Ni,1) )
    di = 2 * ones( (Ni,1) )
    si = arange(Ne, N).reshape(Ni, 1) # indices of the inhibitory units
    
    # combine the Excitatory and Inhibitory parameters into 1 array 
    a = vstack( (ae, ai) )
    b = vstack( (be, bi) )
    c = vstack( (ce, ci) )
    d = vstack( (de, di) )
    s = vstack( (se, si) )
    
    # shuffle up the matrices, so that the e/i units are all mixed together around the ring
    a,b,c,d,s = shuffle(a,b,c,d,s)
    
    # create connection matrix s, each unit connected to k nearest neighbors
    S = zeros( (N, N) )
    
    # connect each node to its k nearest neighbors
    # TODO: should we vectorize this?
    for src in range(N):
        # neighbors is [1, -1, 2, -2, ...]
        neighbors = [item for sublist in zip(range(1,N), range(-1,-N,-1)) for item in sublist]
        for i in neighbors[:k]:
            dst = (src + i) % N
            S[src,dst] = 1.
            
    # rewire connections with probability p
    for src in range(N):
        for dst in range(N):
            # floating point compare to 0, need better method to detect no connection
            if S[src, dst] != 0. and random() < p:  
                rewired = False
                while not rewired:
                    new_dst = randrange(0, N)
                    if S[src, new_dst] == 0. and new_dst != src:
                        w = S[src, dst]
                        S[src, dst] = 0.
                        S[src, new_dst] = w
                        rewired = True

    # use the shuffled indexes to find excitatory and inhibitory units, give appropriate weights to them
    # TODO: should test this more thoroughly, are we getting the e/i weights where we think and in right place?
    ei = where(s<Ne)[0]
    S[:,ei] = S[:, ei] * rand(N, Ne) * we
    ii = where(s>=Ne)[0]
    S[:,ii] = S[:, ii] * -wi * rand(N, Ni)
    
    return a,b,c,d,S,ei,ii




def calc_avg_clustering(w):
    """Given a square numpy weight matrix, calculate network clustering coefficient (C).  Here
    we treat w as an undirected network when we convert into a networkx graph to perform the
    analysis for us.
    """
    w_undir = (w != 0) * 1
    g = nx.from_numpy_matrix(w_undir, create_using=nx.DiGraph())
    g = g.to_undirected()
    C = nx.average_clustering(g)
    return C




def calc_avg_wiring_length(w):
    """Given a square numpy weight matrix, calculate network average wiring length (L).  Here
    we treat w as an undirected network when we convert into a networkx graph to perform the
    analysis for us.
    """
    w_undir = (w != 0) * 1
    g = nx.from_numpy_matrix(w_undir, create_using=nx.DiGraph())
    g = g.to_undirected()
    L = nx.shortest_paths.average_shortest_path_length(g)
    return L




def simulate_network(Ne=800, Ni=200, k=10, p=0.0, we=10, wi=5, te=5, ti=2.5, ts=5000):
    """Given the number of units (Ne = number of excitatory, Ni = number of inhibitory), this function
    creates a network of Izhikevich units.  The units are connected in a regular ring lattice, each unit
    is connected to its k nearest neighbors.  The regular lattice is then interpolated by the 
    Watts & Strogatz algorithm, with probability p each connection is randomly rewired to a new node.
    
    This function simulates the dynamic spiking activity of the resulting network, and returns
    the unit spiking data as a result.  We return the network connectivity matrix S as well for
    some post processing on the network graph.
    """
    N = Ne + Ni # total number of units, excitatory and inhibitory
    a,b,c,d,S,ei,ii = create_wsinterpolated_network(Ne, Ni, k, p, we, wi)
    
    # run simulation for number of time steps
    v = -65 * ones( (N, 1) )    # Initial values of v
    u = b * v                          # Initial values of u
    firings = array([ [0], [0] ])
    
    for t in range(ts):                # simulation of ts ms
        if t % 1000 == 0: print t,
        #I = vstack( ( 5 * randn(Ne, 1), 2 * randn(Ni, 1) ) ) # thalmic input
        I = ones( (N, 1) )
        I[ei] = I[ei] * te * randn(Ne, 1) # thalmic excitation random noise
        I[ii] = I[ii] * ti * randn(Ni, 1) # thalmic inhibition random noise
        fired = where(v >= 30)[0]
        ff = vstack( (t+0*fired, fired) )
        firings = hstack( (firings, ff) )
        v[fired] = c[fired]
        u[fired] = u[fired] + d[fired]
        I = I + sum(S[:, fired], axis=1).reshape((N,1))
        v = v + 0.5 * (0.04 * v**2.0 + 5*v + 140 - u + I) # step 0.5 ms
        v = v + 0.5 * (0.04 * v**2.0 + 5*v + 140 - u + I) # for numerical
        u = u + a * (b * v - u)                           # stability

    print "done"
    
    return firings, S




def draw_firings_activity_plot(f_reg, a_reg, f_sw, a_sw, f_rnd, a_rnd, trange=(3000,5000), arange=(0,50)):
    """Draw a dot plot of firing activity, and a plot of network activity, given 3 sets of firings/activity
    arrays.
    """
    tmin, tmax = trange
    amin, amax = arange
    fig = plt.figure(figsize=(12,8))
    
    # draw the text at the bottom
    axall = fig.add_axes([0.0, 0.0, 1.0, 1.0])
    axall.axis('off')
    axall.text(0.15, 0.05, '$p = 0$', transform=axall.transAxes, fontsize=18)
    axall.text(0.8, 0.05, '$p = 1$', transform=axall.transAxes, fontsize=18)
    axall.text(0.4, 0.03, 'Increasing randomness', transform=axall.transAxes, fontsize=14)
    axall.arrow(0.22, 0.07, 0.53, 0.0, color='black', linewidth=0.5, width=.004, head_width=0.02)
    
    # add figure of the Regular network on the left 1/3
    ax1 = fig.add_axes([0.05, 0.13, 0.3, 0.62])
    #ax1.text(0.39, 1.02, 'Regular', transform=ax1.transAxes, fontsize=18)
    ax1.plot(f_reg[0,:], f_reg[1,:], '.', markersize=1.0, rasterized=True)
    ax1.set_ylabel('Neuron number')
    ax1.set_xticks(range(tmin, tmax, 500))
    #ax1.set_xticklabels([])
    ax1.set_xlabel('time (ms)')
    ax1.set_yticks(range(0, 1001, 100))
    ax1.set_xlim(tmin, tmax)
    ax1.grid('off')
    
    ax4 = fig.add_axes([0.05, 0.77, 0.3, 0.18])
    ax4.plot(range(0, a_reg.shape[0]), a_reg, linewidth=1)
    ax4.set_ylabel('Network activity')
    ax4.set_xticks([0])
    ax4.set_xticklabels([])
    ax4.set_xlim(tmin, tmax)
    ax4.set_ylim(amin, amax)
    ax4.text(0.39, 1.05, 'Regular', transform=ax4.transAxes, fontsize=18)
    ax4.grid('off')
    
    # add figure of the small-world network in middle 
    ax2 = fig.add_axes([0.35, 0.13, 0.3, 0.62])
    #ax2.text(0.39, 1.02, 'Small-World', transform=ax2.transAxes, fontsize=18)
    ax2.plot(f_sw[0,:], f_sw[1,:], '.', markersize=1.0, rasterized=True)
    #ax2.set_ylabel('Neuron number')
    ax2.set_xticks(range(tmin, tmax, 500))
    #ax2.set_xticklabels(['3000', '3500', '4000', '4500']) # kludge, something wrong with set_xticks
    ax2.set_xlabel('time (ms)')
    ax2.set_yticks([0])
    ax2.set_yticklabels([])
    ax2.set_xlim(tmin, tmax)
    ax2.grid('off')
    
    ax5 = fig.add_axes([0.35, 0.77, 0.3, 0.18])
    ax5.plot(range(0, a_sw.shape[0]), a_sw, linewidth=1)
    #ax5.set_ylabel('Network activity')
    ax5.set_yticks([0])
    ax5.set_yticklabels([])
    ax5.set_xticks([0])
    ax5.set_xticklabels([])
    ax5.set_xlim(tmin, tmax)
    ax5.set_ylim(amin, amax)
    ax5.text(0.39, 1.05, 'Small-World', transform=ax5.transAxes, fontsize=18)
    ax5.grid('off')
    
    # add figure of the random network on right 1/3
    ax3 = fig.add_axes([0.65, 0.13, 0.3, 0.62])
    #ax3.text(0.39, 1.02, 'Random', transform=ax3.transAxes, fontsize=18)
    ax3.plot(f_rnd[0,:], f_rnd[1,:], '.', markersize=1.0, rasterized=True)
    #ax3.set_ylabel('Neuron number')
    ax3.set_xticks(range(tmin, tmax+1, 500))
    #ax2.set_xticklabels([])
    ax3.set_xlabel('time (ms)')
    ax3.set_yticks([0])
    ax3.set_yticklabels([])
    ax3.set_xlim(tmin, tmax)
    ax3.grid('off')
    
    ax6 = fig.add_axes([0.65, 0.77, 0.3, 0.18])
    ax6.plot(range(0, a_rnd.shape[0]), a_rnd, linewidth=1)
    #ax6.set_ylabel('Network activity')
    ax6.set_yticks([0])
    ax6.set_yticklabels([])
    ax6.set_xticks([0])
    ax6.set_xticklabels([])
    ax6.set_xlim(tmin, tmax)
    ax6.set_ylim(amin, amax)
    ax6.text(0.39, 1.05, 'Random', transform=ax6.transAxes, fontsize=18)
    ax6.grid('off')




def create_delay_matrix(N, distance_scale=50.):
    """Create an appropriate propogation delay matrix for use with variation of network simulation
    that simulates a wiring length propogation delay.  Matrix is an NXN matrix, with the distance
    from src to dst calculated.  Returns this distance matrix D and the max_delay that is present
    in network
    """
    # create a wiring length delay/distance matrix
    D = zeros( (N,N) )
    max_delay = (N / distance_scale / 2) - 1

    for src in range(N):
        # neighbors is [1, -1, 2, -2, ...]
        neighbors = [item for sublist in zip(range(1,N), range(-1,-N,-1)) for item in sublist]
        for i in neighbors[:N-1]:
            dst = (src+i) % N
            distance = abs(i)
            distance = floor(float(distance)/float(distance_scale))
            if distance > max_delay:
                distance = max_delay
            D[src, dst] = distance

    return D, int(max_delay)




def simulate_network_delay(Ne=800, Ni=200, k=10, p=0.0, we=10, wi=5, te=5, ti=2.5, ts=5000, distance_scale=50.):
    """Given the number of units (Ne = number of excitatory, Ni = number of inhibitory), this function
    creates a network of Izhikevich units.  The units are connected in a regular ring lattice, each unit
    is connected to its k nearest neighbors.  The regular lattice is then interpolated by the 
    Watts & Strogatz algorithm, with probability p each connection is randomly rewired to a new node.
    
    This function simulates the dynamic spiking activity of the resulting network.  This version
    of simulate network also calculates a propogation delay of the spike firings, based on
    distance between src and dst units.  
    
    This function returns the unit spiking data as a result.  We return the network connectivity 
    matrix S as well for some post processing on the network graph.
    """
    N = Ne + Ni # total number of units, excitatory and inhibitory
    a,b,c,d,S,ei,ii = create_wsinterpolated_network(Ne, Ni, k, p, we, wi)
    
    # precompute Delay matrices at different delays, to save some time in main loop
    D, max_delay = create_delay_matrix(N, distance_scale)
    D_precomp = empty( (N, N, max_delay+1) )
    for delay in range(max_delay+1):
        D_precomp[:,:,delay] = (D == delay)
    
    
    # run simulation for number of time steps
    v = -65 * ones( (N, 1) )    # Initial values of v
    u = b * v                          # Initial values of u
    firings = array([ [0], [0] ])
    I = zeros( (N, max_delay+1) )
    
    for t in range(ts):                # simulation of ts ms
        if t % 1000 == 0: print t,
            
        # Input resulting from thalmic excitation of excitatory and inhibitory units
        I[ei,0] = I[ei,0] + (te * randn(Ne)) # thalmic excitation random noise
        I[ii,0] = I[ii,0] + (ti * randn(Ni)) # thalmic inhibition random noise

        fired = where(v >= 30)[0]
        ff = vstack( (t+0*fired, fired) )
        firings = hstack( (firings, ff) )
        v[fired] = c[fired]
        u[fired] = u[fired] + d[fired]

        # calculate Input resulting from firings spread through weights
        # we use delay matrix D to simulate propogation delay of wiring length
        I_firings = repeat(S[:,fired],max_delay+1).reshape( (N,fired.shape[0],max_delay+1) ) * D_precomp[:,fired,:]
        I += sum(I_firings, axis=1)
        
        v = v + 0.5 * (0.04 * v**2.0 + 5*v + 140 - u + I[:,0].reshape(N,1)) # step 0.5 ms
        v = v + 0.5 * (0.04 * v**2.0 + 5*v + 140 - u + I[:,0].reshape(N,1)) # for numerical
        u = u + a * (b * v - u)                                # stability
        
        # shift Input buffers for next time step/ms of simulation
        I[:,0:max_delay] = I[:,1:max_delay+1]
        I[:,max_delay] = zeros( (N,) )
        
    print "done"
    
    return firings, S




def convolve_firings(firings, con_width = 15):
    """Given an array of spike firings, convolve each units spiking activity by the convolution kernel.
    """
    num_ts = max(firings[0,:]) + 1
    num_units = max(firings[1,:]) + 1
    firings_convolved = empty((num_units, num_ts))
    
    # array to convolute against each spike firings time series
    # this is the convolution kernel function that is convolved with the unit firings using scipy.signal.convolve
    x_con = linspace(-con_width, con_width, 2*con_width)
    y_con = exp(-(x_con/10.0)**2.0)
    
    for ui in range(num_units):
        t = zeros(num_ts)
        t[firings[0, firings[1,:] == ui]] = 1.0
        t_con = convolve(t, y_con, mode='full')[con_width:num_ts+con_width]
        firings_convolved[ui,:] = t_con
        
    return firings_convolved




def add_convoluted_firings(f_con):
    """Sum all convoluted signals together in f_con and return result.
    """
    res = f_con[0,:]
    for ui in range(1, f_con.shape[0]):
        res = res + f_con[ui,:]
    return res




def calc_power_spectrum(signal):
    """Calculate and return the power spectrum, and frequency values, of the given signal)"""
    ps = abs(fft.fft(signal))**2
    time_step = 1. / 1000. # hardcoded assumption that our sampling Fs == 1000 / sec or 1 ms
    freqs = fft.fftfreq(signal.size, time_step)
    idx = argsort(freqs)
    return ps, freqs, idx




def max_freq_power(freqs, ps):
    """Given frequency and power spectrum vectors, find where power is maximum, and return
    that power value, with the corresponding frequency value.
    
    Because of the way we calcluate power spectrum, we exclude frequencies around 0, which are always big
    """
    i_max = where(ps == ps[freqs > 1.0].max())
    f_max = freqs[i_max][0]
    p_max = ps[i_max][0]
    
    return f_max, p_max



def exp_script_ready():
    return True

