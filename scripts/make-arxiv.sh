#!/bin/bash
PROJ=synchronization-dynamics-small-world

mkdir arxiv
cd arxiv
cp ../$PROJ.tex .
cp ../$PROJ.bib .
cp ../styles/*.bst .
sed -i 's/\.png//g' $PROJ.tex 
PICS=`grep includegraphics $PROJ.tex | sed 's/.*figures\/\([^}]*\).*/\1/'`

for pic in $PICS
do
    cp ../../figures/$pic.eps .
    # there is a bug in matplotlib .eps generation currently, this removes the
    # postscript line causing the bug.  Remove this when matplotlib/ghostscript get fixed?
    sed -i '/^end$/d' $pic.eps 
done

sed -i 's/\.\.\/figures/\./g' $PROJ.tex

latex $PROJ
bibtex $PROJ
latex $PROJ
latex $PROJ
dvips -t letter -Ppdf -G0 $PROJ
ps2pdf $PROJ.ps

rm -f *.ps *.out *.log *.dvi *.blg *.aux
