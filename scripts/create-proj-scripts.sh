#!/bin/bash

ipython nbconvert notebooks/ProjectScripts.ipynb --to python
mv ProjectScripts.py scripts/exp-scripts.tmp
./scripts/extract-script.py scripts/exp-scripts.tmp > scripts/scripts.py
rm scripts/exp-scripts.tmp

