Introduction
============

This repository contains all scripts, notebooks and artifacts needed
in order to recreate the simulations and data analysis described in
the paper:

Harter, D. Spike synchronization dynamics of small-world networks. (2013)

In this research, we examine the effects of small-world
network organizations on spike synchronization dynamics in networks of
Izhikevich spiking units.  We interpolate network organizations from
regular ring lattices, through the small-world region, to random
networks and measure global spike synchronization dynamics.  We
examine how average path length and clustering may effect the dynamics
of global and neighboring clique spike organization and propogation.
We add additional realistic constraints on the dynamics by introducing
propogation delays of spiking signals proportional to wiring length.
The addition of delays further enhances the finding that small-world
organization may be beneficial for balancing neighborhood synchronized
waves of organization with global synchronization dynamics.

References
==========

[1] Bullmore, E., Sporns, O. Complex brain networks: graph theoretical
    analysis of structural and functional systems. Nature Reviews
    Neuroscience, 10, 186-198 (2009).

[2] Boccaletti, S. Latora, V., Moreno, Y., Chavez, M. & Hwang, D.U.
    Complex networks: structure and dynamics.  Physics Reports, 424
    (4-5), 175-308 (2006).


